@echo off
setlocal

if "%JAVA_HOME%"=="" goto :JAVA_HOME_REQUIRED
if "%MAVEN_HOME%"=="" goto CONFIGURE_MAVEN
goto SHOW_CONFIGURATION

:JAVA_HOME_REQUIRED
echo JAVA_HOME environment variable is undefined. please configure it before continuing...
goto END

:CONFIGURE_MAVEN
set MAVEN_HOME=%~dp0tools\apache-maven-3.2.1
goto SHOW_CONFIGURATION

:SHOW_CONFIGURATION
echo JAVA_HOME                   : %JAVA_HOME%
echo MAVEN_HOME                  : %MAVEN_HOME%
echo.
goto DO_IT

:DO_IT
call %MAVEN_HOME%\bin\mvn test
goto END

:END

endlocal
