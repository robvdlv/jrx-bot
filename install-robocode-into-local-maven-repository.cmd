@echo off
setlocal

set ROBOCODE_VERSION=1.9.2.1
set MAVEN_LIBS_CONFIG_FILE=%~n0.config

if "%JAVA_HOME%"=="" goto :JAVA_HOME_REQUIRED
if "%MAVEN_HOME%"=="" goto CONFIGURE_MAVEN
goto SHOW_CONFIGURATION

:JAVA_HOME_REQUIRED
echo JAVA_HOME environment variable is undefined. please configure it before continuing...
goto END

:CONFIGURE_MAVEN
set MAVEN_HOME=%~dp0tools\apache-maven-3.2.1
goto SHOW_CONFIGURATION

:SHOW_CONFIGURATION
echo Installs robocode development libraries into your local maven repository.
echo.
echo Configure which libs you want to install in the maven libraries config file.
echo Each line in the config file defines the jar file and its maven artifact id:
echo.
echo.  ^<jar-file-name^>,^<artifact-id^>
echo.
echo For example:
echo.
echo   robocode.core-1.9.2.1.jar,robocode-core
echo.
echo Current configuration:
echo.
echo   robocode version            : %ROBOCODE_VERSION%
echo   maven libraries config file : %MAVEN_LIBS_CONFIG_FILE%
echo   JAVA_HOME                   : %JAVA_HOME%
echo   MAVEN_HOME                  : %MAVEN_HOME%
echo.
echo Press ENTER to continue the installation of the libraries into your local maven repository...
echo Press CTRL+C to abort...
pause >nul
echo.
goto DO_IT

:DO_IT
echo Installing libraries defined in %MAVEN_LIBS_CONFIG_FILE%...
echo.
FOR /F "tokens=1,2 delims=," %%g IN (%MAVEN_LIBS_CONFIG_FILE%) DO (
  call %MAVEN_HOME%\bin\mvn install:install-file -Dfile=lib\%%g -DartifactId=%%h -DgroupId=net.sourceforge.robocode -Dversion=%ROBOCODE_VERSION% -Dpackaging=jar -DgeneratePom=true
)
goto END

:END

endlocal
