package bot.commands;

import bot.Command;
import robocode.Robot;

public class Turn implements Command {

    private final double degrees;

    public Turn(double degrees) {
        this.degrees = degrees;
    }

    @Override
    public void Execute(Robot robot) {
        if (degrees < 0){
            robot.turnLeft(9);
        } else if (degrees > 0) {
            robot.turnRight(9);
        }
    }
}
