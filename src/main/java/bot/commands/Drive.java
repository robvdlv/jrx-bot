package bot.commands;

import bot.Command;
import robocode.Robot;

public class Drive implements Command {
    private final double distance;

    public Drive(double distance) {
        this.distance = distance;
    }

    @Override
    public void Execute(Robot robot) {
        robot.ahead(distance);
    }
}
