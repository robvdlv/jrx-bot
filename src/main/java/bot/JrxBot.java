package bot;

import bot.commands.Drive;
import bot.commands.Turn;
import plumbing.ILog;
import plumbing.Log;
import robocode.*;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;

import java.util.Queue;

public class JrxBot extends Robot {

    private final ILog log = Log.New(JrxBot.class);

    private final RxEventEmitter rxEventEmitter = new RxEventEmitter();
    private Command current;
    //private final Set<Subscription> subscriptions = new CopyOnWriteArraySet<Subscription>();

    public void run() {
        // create an observable event stream;
        // each new subscriber will be receiving events emitted by the robocode events to rx subscriber adapter
        Observable<Event> events = Observable.create(new Observable.OnSubscribe<Event>() {
            @Override
            public void call(final Subscriber<? super Event> subscriber) {
                rxEventEmitter.subscribe(subscriber);
            }
        });

        //events.ofType(HitByBulletEvent.class).subscribe(new MoveBasedOnHitOrNot(commands));

        events.ofType(ScannedRobotEvent.class)
                .subscribe(new Action1<ScannedRobotEvent>() {
                    @Override
                    public void call(final ScannedRobotEvent event) {
                        current = new Command() {
                            @Override
                            public void Execute(Robot robot) {
                                new Turn(event.getBearing()).Execute(robot);
                                new Drive(10).Execute(robot);
                            }
                        };
                    }
                });

        while (true) {
            if (current != null){
                current.Execute(this);
                current = null;
            } else {
//                turnRight(18);
//                turnRadarRight(18);
            }
        }
    }

    @Override
    public void onDeath(DeathEvent event) {
        rxEventEmitter.onDeath(event);
    }

    public void onScannedRobot(ScannedRobotEvent event) {
        rxEventEmitter.onScannedRobot(event);
    }

    public void onHitByBullet(HitByBulletEvent event) {
        rxEventEmitter.onHitByBullet(event);
    }

    @Override
    public void onBattleEnded(BattleEndedEvent event) {
        rxEventEmitter.onBattleEnded(event);
    }

    private class MoveBasedOnHitOrNot implements Action1<HitByBulletEvent> {

        private final Queue<Command> commands;

        public MoveBasedOnHitOrNot(Queue<Command> commands) {
            this.commands = commands;
        }

        @Override
        public void call(HitByBulletEvent hitByBulletEvents) {
            log.write("got hit!");
            commands.offer(new Command() {
                @Override
                public void Execute(Robot bot) {
                    bot.turnRight(45);
                    bot.ahead(100);
                }
            });
        }
    }
}
