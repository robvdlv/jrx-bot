package bot;

import robocode.robotinterfaces.IBasicEvents;
import robocode.robotinterfaces.IBasicEvents2;

public interface IBotEvents extends IBasicEvents, IBasicEvents2 {
}
