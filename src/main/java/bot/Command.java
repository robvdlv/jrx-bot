package bot;

import robocode.Robot;

public interface Command {
    void Execute(Robot robot);
}
