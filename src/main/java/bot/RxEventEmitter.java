package bot;

import plumbing.ILog;
import plumbing.Log;
import robocode.*;
import robocode.robotinterfaces.IBasicEvents2;
import rx.Subscriber;

import java.util.concurrent.CopyOnWriteArrayList;

public class RxEventEmitter implements IBasicEvents2 {

    private final ILog log = Log.New(RxEventEmitter.class);
    private final CopyOnWriteArrayList<Subscriber<? super Event>> subscribers = new CopyOnWriteArrayList<Subscriber<? super Event>>();

    public void subscribe(Subscriber<? super Event> subscriber) {
        if (subscriber == null)
            throw new IllegalArgumentException("subscriber");

        subscribers.add(subscriber);
        log.write("subscribed [%s]", subscriber);
    }

    @Override
    public void onStatus(StatusEvent event) {
        onNext(event);
    }

    @Override
    public void onBulletHit(BulletHitEvent event) {
        onNext(event);
    }

    @Override
    public void onBulletHitBullet(BulletHitBulletEvent event) {
        onNext(event);
    }

    @Override
    public void onBulletMissed(BulletMissedEvent event) {
        onNext(event);
    }

    @Override
    public void onDeath(DeathEvent event) {
        onNext(event);
    }

    @Override
    public void onHitByBullet(HitByBulletEvent event) {
        onNext(event);
    }

    @Override
    public void onHitRobot(HitRobotEvent event) {
        onNext(event);
    }

    @Override
    public void onHitWall(HitWallEvent event) {
        onNext(event);
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
        onNext(event);
    }

    @Override
    public void onRobotDeath(RobotDeathEvent event) {
        onNext(event);
    }

    @Override
    public void onWin(WinEvent event) {
        onNext(event);
    }

    @Override
    public void onBattleEnded(BattleEndedEvent event) {
        onNext(event);
    }

    private void onNext(Event event) {
        for (Subscriber<? super Event> subscriber : subscribers) {
            subscriber.onNext(event);
        }
    }
}
