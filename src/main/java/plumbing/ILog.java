package plumbing;

public interface ILog {
    void write(Object message);
    void write(String messageFormat, Object... args);
}
