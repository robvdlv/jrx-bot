package plumbing;

public class Log {

    public static ILog New(Class clazz){
        return New(clazz.getTypeName());
    }

    public static ILog New(String name){
        return new ConsoleLog(name);
    }
}
