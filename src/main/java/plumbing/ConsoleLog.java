package plumbing;

import java.util.Date;

public class ConsoleLog implements ILog {

    private final String name;

    public ConsoleLog(String name) {
        if (name == null)
            throw new IllegalArgumentException("name must not be null");
        this.name = name;
    }

    @Override
    public void write(Object message) {
        doWrite(message.toString());
    }

    @Override
    public void write(String messageFormat, Object... args) {
        doWrite(String.format(messageFormat, args));
    }

    private void doWrite(String message) {
        System.out.printf("(%s) %s [%s] %s%n", Thread.currentThread().getId(), new Date(), name, message);
    }
}
