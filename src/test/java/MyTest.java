import bot.RxEventEmitter;
import org.junit.Test;
import plumbing.ILog;
import plumbing.Log;
import robocode.Event;
import robocode.WinEvent;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MyTest {

    private final ILog log = Log.New(MyTest.class);

    @Test
    public void it() {
        final RxEventEmitter eventsToSubscriberAdapter = new RxEventEmitter();
        Observable<Event> events = Observable.create(new Observable.OnSubscribe<Event>() {
            @Override
            public void call(final Subscriber<? super Event> subscriber) {
                eventsToSubscriberAdapter.subscribe(subscriber);
            }
        }).observeOn(Schedulers.newThread());

        events.subscribe(new Action1<Event>() {
            @Override
            public void call(Event event) {
                log.write("caught event %s", event);
            }
        });

        eventsToSubscriberAdapter.onWin(new WinEvent());

    }
}
